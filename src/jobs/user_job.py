import json
import os
import sys
import time

import redis

from src.actions.sign_in_to_game import sign_in_to_game
from src.actions.types import ActionType
from src.utils.logging import get_logger
from src.utils.selenium import initialize_driver

USERS_FOLDER = os.path.join(os.path.dirname(__file__), '../../data/users_timestamps')



def main(game_id, email, password):
    custom_logger = get_logger(email)

    publisher = redis.Redis(host='localhost', port=6379, db=0)
    subscriber = publisher.pubsub()
    subscriber.subscribe(ActionType.job.value)
    subscriber.subscribe(ActionType.move_card.value)
    subscriber.subscribe(ActionType.set_estimation.value)
    driver = initialize_driver()
    while True:
        msg = subscriber.get_message(ignore_subscribe_messages=True, timeout=0.1)
        if msg is None:
            continue
        if msg["channel"].decode("utf-8") == ActionType.job.value and msg["data"].decode("utf-8") == "START":
            sign_in_to_game(driver, game_id, email, password)
            driver.execute_script('''
                function logEvent(cardId, state){
                    console.log("PYTHON_LOG", cardId, JSON.stringify(state), Date.now());
                }
                window.logEvent = logEvent;
            ''')
            publisher.publish(ActionType.sign_in.value, email)
            custom_logger.info(f"User {email} signed in")
        if msg["channel"].decode("utf-8") == ActionType.job.value and msg["data"].decode("utf-8") == "END":
            time.sleep(2)
            custom_logger.info(f"Terminating user job")
            break
    with open(os.path.join(USERS_FOLDER, f"{email}.json"), 'w') as f:
        f.write(json.dumps(driver.get_log('browser')))
        f.close()
    driver.quit()


if __name__ == '__main__':
    game_id_to_join = sys.argv[1]
    user_email = sys.argv[2]
    user_password = sys.argv[3]
    main(game_id_to_join, user_email, user_password)
