import redis

pub = redis.Redis(host='localhost', port=6379, db=0)
subscriber = pub.pubsub()
subscriber.subscribe('cards_to_await_for')

for item in subscriber.listen():
    print(item)

    if item['data'] == b"DISCONNECT":
        print("Disconnecting")
        subscriber.unsubscribe('cards_to_await_for')
        break
