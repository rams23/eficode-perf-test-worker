import time

from src.actions.move_card import move_card_randomly
from src.actions.set_estimation import set_estimation_to_card
from src.actions.sign_in_to_game import sign_in_to_game
from src.utils.selenium import initialize_driver

my_driver = initialize_driver()

sign_in_to_game(my_driver, 'IzoUuBclbVnK1r4rvvPP', 'test@test.com', 'Test1234')

my_driver.find_element_by_id('zoom-dial').click()
time.sleep(1)
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('fit-window-button').click()
my_driver.execute_script('''
    function logEvent(cardId, state){
        console.log("PYTHON_LOG", cardId, JSON.stringify(state), Date.now());
    }
    window.logFacilitatorEvent = logEvent;
''')
# move_card_by(my_driver, '9tzSep5ORXJoylxVlb6M', -200, -200)
# time.sleep(2)
# move_card_by(my_driver, 'gFDFKdnMmbwL5Vt7ouDE', -500, -200)

# http://localhost:3000/game/U0zLyWj74a5AihiukLOI
time.sleep(2)

move_card_randomly(my_driver, 'sAgOgpSk0phDUfJ7d3Q7')
timestamp = int(time.time() * 1000.0)
print(f"moving card sAgOgpSk0phDUfJ7d3Q7: {timestamp}")
logs =  [l for l in my_driver.get_log('browser') if 'PYTHON_LOG' in l['message']]
print("end")
time.sleep(2)

set_estimation_to_card(my_driver, 'sAgOgpSk0phDUfJ7d3Q7', 'estimation-test')
timestamp = int(time.time() * 1000.0)
print(f"estimation time estimation-test: {timestamp}")