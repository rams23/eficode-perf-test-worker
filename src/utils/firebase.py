import math
import random

import firebase_admin
from firebase_admin import auth
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin.auth import UserRecord
from google.cloud.firestore_v1 import ExistsOption, DocumentSnapshot
from google.cloud.firestore_v1.client import Client
from google.cloud.firestore_v1.types import WriteResult


def get_random_email():
    return f"testEmail{math.floor(random.randint(1, 100000))}@pythonsdk.com".lower()


def create_user(client, email=get_random_email(), email_verified=True, password='t35tpsvv55!') -> UserRecord:
    # this operation returns when the operations succeeds, or raises an exception if it fails somehow.
    new_user: UserRecord = auth.create_user(email=email, email_verified=email_verified, password=password)
    print(f"Firebase successfully created a new user with \n\temail:'{new_user.email}'\n\tuser id:'{new_user.uid}'")

    result: WriteResult = client.document(get_user_doc_path(new_user.uid)).set({
        "email": new_user.email,
        "role": 'endUser',
        "devOpsMaturity": 'veryImmature'
    })

    print(result.update_time)

    found_doc: DocumentSnapshot = client.document(get_user_doc_path(new_user.uid)).get()
    if not found_doc.exists:
        raise Exception("Error while writing user data to firestore.")

    return new_user


def remove_user(user_id: str, firestore_cl: Client, delete_firestore=True, delete_auth=True):
    """
    Both 'delete' operations fail if the document (for firestore) and the user (for auth) do not exist.
    """
    print(f"Removing user '{user_id}'")
    if delete_firestore:
        firestore_cl.document(get_user_doc_path(user_id)).delete(ExistsOption(exists=True))
    if delete_auth:
        auth.delete_user(user_id)


def get_user_doc_path(user_id: str):
    return f"users/${user_id}"


def initialize_admin_sdk():
    app = firebase_admin.initialize_app()
    firestore_client: Client = firestore.client(app)
    return app, firestore_client
