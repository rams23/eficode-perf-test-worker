import json
import os
import sys
import time
from datetime import datetime

import redis
from selenium.webdriver.chrome.webdriver import WebDriver

from src.actions.sign_in_to_game import sign_in_to_game
from src.actions.types import ActionType
from src.game_definition import get_game_definition
from src.utils.logging import get_logger
from src.utils.selenium import initialize_driver

EVENTS_SEQUENCE = os.path.join(os.path.dirname(__file__), '../../data/event-sequence.json')
FACILITATOR_LOGS = os.path.join(os.path.dirname(__file__), '../../data/facilitator-logs.json')

custom_logger = get_logger('facilitator')


def try_really_really_hard(f, attempts=10):
    try:
        f()
    except BaseException as e:
        if attempts == 0:
            raise e
        custom_logger.error(str(e))
        try_really_really_hard(f, attempts - 1)


def get_final_state(driver: WebDriver, card_id):
    card = driver.find_element_by_xpath(f"//div[@data-cy='card-{card_id}']")
    position = {
        'left': float(card.value_of_css_property("left").replace('px', '')),
        'top': float(card.value_of_css_property("top").replace('px', ''))
    }
    try:
        estimation_container = driver.find_element_by_id(f"card-estimation-{card_id}")
        estimation = estimation_container.text
    except:
        estimation = ''
    return {'position': position, 'estimation': estimation, 'card_id': card_id}


def zoom_out(driver):
    driver.find_element_by_id('zoom-dial').click()
    time.sleep(1)
    for _ in range(8):
        driver.find_element_by_id('zoom-out-button').click()
    driver.find_element_by_id('fit-window-button').click()
    driver.find_element_by_id('zoom-dial').click()


def main(number_of_users, game_id, email, password):
    publisher = redis.Redis(host='localhost', port=6379, db=0)
    subscriber = publisher.pubsub()
    driver = setup_facilitator(game_id, email, password)
    zoom_out(driver)
    subscriber.subscribe(ActionType.sign_in.value)
    publisher.publish(ActionType.job.value, "START")
    # todo timeout to start game (what if a user crashes)
    wait_sign_in(number_of_users, subscriber)

    events = {}

    N_ACTIONS = 10
    try:
        for action in get_game_definition(N_ACTIONS):
            driver.execute_script(f'''
                window.logFacilitatorEvent = function (cardId, state){{
                    console.log("PYTHON_LOG", cardId, JSON.stringify(state), Date.now(), "{action['type'].value}");
                }};
            ''')
            try:
                try_really_really_hard(lambda: action["execute"](driver), attempts=5)
            except BaseException as e:
                custom_logger.error(e)
                custom_logger.info("Error while performing action, trying next one")
                continue
            timestamp_python_1 = int(time.time() * 1000.0)
            timestamp = driver.execute_script("""return Date.now()""")
            timestamp_python_2 = int(time.time() * 1000.0)

            custom_logger.info(f"{timestamp_python_1}, {timestamp}, {timestamp_python_2}")

            final_state = get_final_state(driver, action['scope']['card'])
            final_state['event_type'] = action['type'].value
            events[timestamp] = final_state
            custom_logger.info(
                f"Facilitator performed {action['type'].value} on {action['scope']['card']} at "
                f"{datetime.utcnow().isoformat(sep=' ', timespec='microseconds')}")
            time.sleep(2)
    except BaseException as e:
        custom_logger.error(e)

    publisher.publish(ActionType.job.value, "END")

    write_events_to_file(events, driver)
    driver.quit()

    custom_logger.info("Facilitator out!")


def write_events_to_file(events, driver):
    with open(EVENTS_SEQUENCE, 'w') as f:
        f.write(json.dumps(events))

    with open(FACILITATOR_LOGS, 'w') as f:
        f.write(json.dumps(driver.get_log('browser')))
        f.close()


def setup_facilitator(game_id, email, password):
    driver = initialize_driver()
    sign_in_to_game(driver, game_id, email, password)
    return driver


def wait_sign_in(n_users, subscriber):
    signed_users = 0
    custom_logger.info("Waiting for users to sign in")
    while signed_users < n_users:
        msg = subscriber.get_message(ignore_subscribe_messages=True)
        if msg and msg["channel"].decode("utf-8") == ActionType.sign_in.value:
            signed_users += 1
        time.sleep(.1)
    custom_logger.info("All users signed in")
    subscriber.unsubscribe("sign_in")


if __name__ == '__main__':
    n_users = int(sys.argv[1])
    game_id_to_join = sys.argv[2]
    facilitator_email = sys.argv[3]
    facilitator_password = sys.argv[4]
    main(n_users, game_id_to_join, facilitator_email, facilitator_password)
