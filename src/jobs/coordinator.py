import json
import shutil
import subprocess
import sys
import uuid
import time
import os
from subprocess import Popen
from firebase_admin import auth
from firebase_admin import firestore
import firebase_admin
from google.cloud.firestore_v1.client import Client
from selenium.webdriver.chrome.webdriver import WebDriver

from src.actions.create_game import create_game
from src.actions.sign_up_to_game import sign_up_to_game, execute_sign_up
from src.actions.sign_in_to_game import sign_in_to_game
from src.utils.logging import get_logger
from src.utils.firebase import initialize_admin_sdk
from src.utils.game import get_base_url
from src.utils.selenium import initialize_driver
from threading import Thread

METRIC_FILE = os.path.join(os.path.dirname(__file__), '../../data/metrics.json')
EVENTS_SEQUENCE = os.path.join(os.path.dirname(__file__), '../../data/event-sequence.json')
ALL_USERS = os.path.join(os.path.dirname(__file__), '../../data/all-users.json')
USERS_TIMESTAMPS_FOLDER = os.path.join(os.path.dirname(__file__), '../../data/users_timestamps')
LOG_DIR = os.path.join(os.path.dirname(__file__), '../../logs')
FACILITATOR_LOGS = os.path.join(os.path.dirname(__file__), '../../data/facilitator-logs.json')


def sign_up_calculating_time(user_info, game_id, results, custom_logger):
    driver: WebDriver = initialize_driver()
    start_time = time.time()
    sign_up_to_game(driver, game_id, user_info['email'], user_info['password'], custom_logger)
    end_time = time.time()
    results[user_info['email']] = end_time - start_time
    custom_logger.info(f"User {user_info['email']} signed up successfully")
    driver.quit()


def run_game_test(facilitator, users, game_id):
    facilitator_path = os.path.join(os.path.dirname(__file__), './facilitator_job.py')
    user_path = os.path.join(os.path.dirname(__file__), './user_job.py')
    facilitator = Popen([
        sys.executable, facilitator_path,
        str(len(users)), game_id, facilitator['email'], facilitator['password']], stdout=subprocess.PIPE)
    map(lambda p: p.wait(),
        [Popen([sys.executable, user_path, game_id, u['email'], u['password']], stdout=subprocess.PIPE) for u in users])
    facilitator.wait()


def sign_in_calculating_time(user_info, game_id, results, custom_logger):
    driver = initialize_driver()
    custom_logger.info(f"Signing in with user {user_info['email']}")
    start_time = time.time()
    sign_in_to_game(driver, game_id, user_info['email'], user_info['password'])
    end_time = time.time()
    results[user_info['email']] = end_time - start_time
    custom_logger.info(f"User {user_info['email']} signed in successfully")
    driver.quit()


def write_metrics(metrics):
    with open(METRIC_FILE, 'w') as f:
        f.write(json.dumps(metrics))
        f.close()


# {'location': location, 'estimation': estimation, 'card_id': card_id}
def compute_event_id(state):
    card_id = state['card_id']
    estimation = state['estimation'] if 'estimation' in state and state['estimation'] else ''
    position = state['position'] if 'position' in state else {'left': 0, 'top': 0}

    return f"{card_id}-{estimation}-{'{:.0f}'.format(position['left'])}-{'{:.0f}'.format(position['top'])}"


def get_events_timestamps_from_logs(logs):
    relevant_logs = [l for l in logs if 'PYTHON_LOG' in l['message']]

    events_timestamps = dict()
    events_types = dict()
    for l in relevant_logs:
        parts = l['message'].split()
        card_id = parts[3].strip('"')
        json_str = parts[4].strip('"').replace('\\', '')
        timestamp = int(parts[5])
        try:
            event_type = parts[6].strip('"')
        except IndexError:
            event_type = ''
        state = json.loads(json_str)
        if 'position' in state:
            state['position']['left'] = state['position']['x']
            state['position']['top'] = state['position']['y']
        state['card_id'] = card_id
        event_id = compute_event_id(state)
        if event_id not in events_timestamps:
            events_timestamps[event_id] = timestamp
        if event_id not in events_types:
            events_types[event_id] = event_type

    return events_timestamps, events_types


def calculate_metrics(metrics):
    with open(FACILITATOR_LOGS, 'r') as f:
        facilitator_logs = json.load(f)

        facilitator_timestamps, events_types = get_events_timestamps_from_logs(facilitator_logs)

        files_paths = [os.path.join(USERS_TIMESTAMPS_FOLDER, f) for f in os.listdir(USERS_TIMESTAMPS_FOLDER) if
                       os.path.isfile(os.path.join(USERS_TIMESTAMPS_FOLDER, f)) and '.json' in f]

        all_users_timestamps = dict()
        for user_file_name in files_paths:
            with open(user_file_name, 'r') as user_file:
                user_logs = json.load(user_file)

                user_events_timestamps, _ = get_events_timestamps_from_logs(user_logs)

                all_users_timestamps[user_file_name] = user_events_timestamps

                for event_id in facilitator_timestamps.keys():

                    event_type = events_types[event_id]

                    event_fired_at = int(float(facilitator_timestamps[event_id]))

                    if event_id in user_events_timestamps:

                        received_at = user_events_timestamps[event_id]
                        delta = (received_at - event_fired_at) / 1000
                    else:
                        delta = None

                    if event_type not in metrics:
                        metrics[event_type] = []

                    metrics[event_type].append(delta)

        with open(ALL_USERS, 'w') as all_users:
            all_users.write(json.dumps(all_users_timestamps))
            all_users.close()


def calculate_signup_metrics(game_id, number_of_users, users, custom_logger):
    sign_up_threads = [None] * number_of_users
    signup_times = {}
    for i in range(len(sign_up_threads)):
        thread = Thread(target=sign_up_calculating_time, args=(users[i], game_id, signup_times, custom_logger))
        sign_up_threads[i] = thread
        thread.start()
    for i in range(len(sign_up_threads)):
        sign_up_threads[i].join()
    deltas = list(signup_times.values())
    return deltas


def calculate_sign_in_metrics(game_id, number_of_users, users, custom_logger):
    # signin_to_game metrics
    sign_in_threads = [None] * number_of_users
    signin_times = {}
    for i in range(len(sign_in_threads)):
        thread = Thread(target=sign_in_calculating_time, args=(users[i], game_id, signin_times, custom_logger))
        sign_in_threads[i] = thread
        thread.start()
    for i in range(len(sign_in_threads)):
        sign_in_threads[i].join()
    deltas = list(signin_times.values())
    return deltas


def clean_up_users(users):
    firestore_client: Client = firestore.client(firebase_admin.get_app())
    for u in users:
        user = auth.get_user_by_email(u['email'])
        firestore_client.document(f"users/{user.uid}").delete()
        auth.delete_user(user.uid)


def run_tests(number_of_users: int):
    cleanup_user_files()
    custom_logger = get_logger('coordinator')
    custom_logger.info(f"Running with {n_users} users")

    initialize_admin_sdk()

    # create facilitator user
    facilitator_info = create_facilitator_user()

    # create game

    game_id = create_game_to_play(facilitator_info, custom_logger)

    metrics = {}

    users = [{'email': f"{uuid.uuid4()}@perf-test.com", 'password': 'Test1234'} for _ in range(number_of_users)]

    # signup_to_game metrics

    metrics['sign-up'] = calculate_signup_metrics(game_id, number_of_users, users, custom_logger)

    # signin_to_game metrics

    metrics['sign-in'] = calculate_sign_in_metrics(game_id, number_of_users, users, custom_logger)

    # run facilitator job and usr jobs passing user info

    run_game_test(facilitator_info, users, game_id)

    users.append(facilitator_info)
    clean_up_users(users)

    # calculate metrics
    calculate_metrics(metrics)

    write_metrics(metrics)

def clean_dir(dir):
    for filename in os.listdir(dir):
        filepath = os.path.join(dir, filename)
        try:
            shutil.rmtree(filepath)
        except OSError:
            os.remove(filepath)


def cleanup_user_files():
    clean_dir(USERS_TIMESTAMPS_FOLDER)
    clean_dir(LOG_DIR)



def create_game_to_play(facilitator_info, custom_logger):
    driver1 = initialize_driver()
    create_game_id = create_game(driver1, facilitator_info['email'], facilitator_info['password'], custom_logger)
    driver1.close()
    return create_game_id


def create_facilitator_user():
    password = 'Test1234'
    facilitator_email = f"{uuid.uuid4()}@perf-test.com"
    facilitator_info = {
        'email': facilitator_email,
        'password': password,
    }
    driver = initialize_driver()
    execute_sign_up(driver, facilitator_email, password, get_base_url())
    driver.quit()
    return facilitator_info


if __name__ == '__main__':

    try:
        n_users = int(sys.argv[1])
    except IndexError:
        n_users = 10

    run_tests(n_users)
