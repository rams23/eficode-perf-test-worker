import time

import redis

publisher = redis.Redis(host='localhost', port=6379, db=0)
TOPIC = 'cards_to_await_for'

cards_to_dispatch = [312, 1231, 11]
for card in cards_to_dispatch:
    publisher.publish('cards_to_await_for', card)
    time.sleep(2)

publisher.publish('cards_to_await_for', "DISCONNECT")
