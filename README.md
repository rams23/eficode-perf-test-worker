## Worker repository:
The performance test consists of 2 repositories:
- Orchestrator
- Worker

**This is the worker**.

The worker will expose the following endpoints:
```
/            Used to verify that the worker is up and running
/start       Used to tell the worker that it can start the test
/metrics     It returns the JSON with the metrics if the test is finished
```

The /metrics endpoint (if the test has ended) will answer with a JSON of this kind:
```
{
  "game-id": "<game-id>",
  "sign-in": [...],
  "sign-up": [...],
  "create-game": [...],
  "card-moved": [...],
  "joined-game": [...],
  "add-estimation": [...],
}
```
where [...] indicates a list of **integer** values indicating the milliseconds for each measured delta.

---

The worker applications consists of the following parts:
- Flask web framework to expose endpoints.
- Selenium web driver (dockerized) to interact with the game UI
- Redis (dockerized) as message broker and in-memory store to enable communication between Selenium processes and to store the metrics
- Firebase Admin SDK to interact with the Firebase backend



---


The test can be summarized as follows:
1. The orchestrator setups the Cloud infrastructure and instatiates the VMs
2. Each VM will setup itself through a startup script
3. The startup script will use the docker-compose to start Redis and the Selenium standalone server, and then setup the endpoints through Flask
4. When the /start endpoint will be called, the test will start.
5. If the test is finished, the /metrics endpoint will provide the measurements.


#ENV

GOOGLE_APPLICATION_CREDENTIALS="absolute/path/to/your/credentials.json"
USE_LOCAL_SELENIUM=true

