from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import presence_of_element_located, element_to_be_clickable

from selenium.webdriver.support.wait import WebDriverWait

from src.actions.sign_in_to_game import execute_sign_in
from src.utils.game import get_base_url


def create_game(driver: WebDriver, email: str, password: str, custom_logger):
    """
    Email and password should belong to an already registered user
    """
    url_to_visit = get_base_url()
    driver.get(url_to_visit)

    custom_logger.info(f"Creating game at {url_to_visit}...")

    execute_sign_in(driver, email, password)

    wait = WebDriverWait(driver, 100, poll_frequency=0.1)
    create_game_button = wait.until(presence_of_element_located((By.ID, "go-to-create-game-button")))
    create_game_button.click()

    wait.until(presence_of_element_located((By.ID, "scenarioTitle")))
    wait.until(presence_of_element_located((By.ID, "scenarioContent")))
    driver.find_element_by_id('scenarioTitle').send_keys('Test scenario title')
    driver.find_element_by_id('scenarioContent').send_keys('Test scenario content')

    wait.until(presence_of_element_located((By.ID, "create-game-button")))
    wait.until(element_to_be_clickable((By.ID, "create-game-button")))
    sign_in_button = driver.find_element_by_id("create-game-button")
    sign_in_button.click()

    wait.until(element_to_be_clickable((By.XPATH, "//div[@data-cy='card-sAgOgpSk0phDUfJ7d3Q7']")))
    created_game_id = driver.current_url.split('/')[-1]
    custom_logger.info(f"Created game with id {created_game_id}")
    return created_game_id
