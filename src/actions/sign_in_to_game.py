from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from src.utils.game import get_game_url


def execute_sign_in(driver, email, password):
    wait = WebDriverWait(driver, 10, poll_frequency=0.1)
    wait.until(EC.presence_of_element_located((By.ID, "email")))
    email_form = driver.find_element_by_id('email')
    email_form.send_keys(email)
    psw_form = driver.find_element_by_id('password')
    psw_form.send_keys(password)
    sign_in_button = driver.find_element_by_xpath("//button[@type='submit']")
    sign_in_button.click()


def sign_in_to_game(driver: WebDriver, game_id: str, email: str, password: str):
    url_to_visit = get_game_url(game_id)
    driver.get(url_to_visit)
    execute_sign_in(driver, email, password)

    driver.execute_script('''
        window.isPerfTestRunning = true;
    ''')

    wait = WebDriverWait(driver, 60, poll_frequency=0.1)
    wait.until(EC.presence_of_element_located((By.XPATH, "//div[@data-cy='card-sAgOgpSk0phDUfJ7d3Q7']")))
