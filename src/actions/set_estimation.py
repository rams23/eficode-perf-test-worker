import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support.wait import WebDriverWait


def set_estimation_to_card(driver: WebDriver, card_id: str, estimation: str):
    card = driver.find_element_by_xpath(f"//div[@data-cy='card-{card_id}']")
    action = ActionChains(driver)
    action \
        .double_click(card) \
        .pause(1) \
        .send_keys(*[Keys.BACKSPACE for _ in range(0, 50)]) \
        .pause(0.2) \
        .send_keys(estimation) \
        .perform()
    save_estimation = driver.find_element_by_xpath(f"//div[@data-cy='card-{card_id}']//button[@type='submit']")
    save_estimation.click()


def listen_to_estimation_set(driver: WebDriver, card_id: str, estimation: str):
    wait = WebDriverWait(driver, 100, poll_frequency=0.1, ignored_exceptions=[StaleElementReferenceException])
    wait.until(EC.element_to_be_clickable(
        (By.XPATH, f"//div[@data-cy='card-{card_id}']//div[contains(string(), '{estimation}')]"))
    )
