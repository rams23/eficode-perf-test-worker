# BASE_URL = "http://localhost:3000"
BASE_URL = "https://pipeline-game-dev.firebaseapp.com"


def get_game_url(game_id: str):
    return f"{BASE_URL}/game/{game_id}"


def get_base_url():
    return BASE_URL


def get_card_ids():
    return ["sAgOgpSk0phDUfJ7d3Q7", "vpttIdLCr7AEwNyFv75m", "gFDFKdnMmbwL5Vt7ouDE", "oMSlvF0Vn5Bt0F9YYZbQ",
            "4wEMpQdP5N52iurAkiZR", "7MpHg7z6NSNKkcNmY11a", "5Hq5DBmvxTtsFyto0weW", "bG1qFmdarVbhXquuWkly",
            "Q8339NFFpyhfDz83s0ke", "EWGhSgDp444Bvx90TI9W", "7tuSNJdQmKPnGIaqrf6u", "KRdlU3yCjCDu7LXDDkkQ",
            "aqmtzNJsJ2Rq0amtNaVC", "9tzSep5ORXJoylxVlb6M", "Smxjkq3JZNEJ7r3zVfyK", "EoORqrovfdC02W0GWKyb",
            "euc9onmGXFCKz4EMXAh2", "15tVTz9XKHPxagkBwFLD", "TdUxJ7APAa8mHfANtlRn", "16c8sWQTSXcXoFx3QJCe",
            "oW2c37tpOK3qYQWdb9M7", "6IqpaCa4X9RfXlJS9WE3", "ST0wgNZhTclzqSRizg0Y", "gkCXoVnzA0r3TM3xJZsh",
            "KdqSH6RykC4qsYENGpnh", "V3KgW196nrobZiQXAL5g", "4OLftJ9cS5lJ49xtdUzT"]
