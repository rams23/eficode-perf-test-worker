import json
import os
import sys
from subprocess import Popen

from flask import Flask, request

app = Flask(__name__)

LOCK_FILE = os.path.join(os.path.dirname(__file__), '../data/run.lock')
METRICS_FILE = os.path.join(os.path.dirname(__file__), '../data/metrics.json')


@app.route('/')
def hello_world():
    return {'status': 'OK'}


@app.route('/start', methods=['POST'])
def start():
    if os.path.isfile(LOCK_FILE):
        return "", 409
    with open(LOCK_FILE, 'w') as f:
        content = request.json
        job_path = os.path.join(os.path.dirname(__file__), 'jobs', 'coordinator.py')
        Popen([sys.executable, job_path, str(content['n_users'])])
        return "", 202


@app.route('/metrics')
def shutdown():
    try:
        with open(METRICS_FILE, 'r') as f:
            return json.load(f)
    except:
        return "", 503


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=9090)
