from random import shuffle

from src.actions.move_card import move_card_randomly
from src.actions.set_estimation import set_estimation_to_card
from src.actions.sign_in_to_game import sign_in_to_game
from src.utils.game import get_card_ids
from src.utils.logging import logger
from src.utils.selenium import initialize_driver


def main():
    global driver, card
    driver = initialize_driver()
    sign_in_to_game(driver, 'J7pPJr7L7AuJlqa7MGDh', 'gabriele.m1995@gmail.com', 'Testtest123!')
    cards = [c for c in get_card_ids() for _ in range(0, 10)]
    shuffle(cards)
    for card in cards[:20]:
        move_card_or_retry(card)
        set_estimation_to_card(driver, card, "miao")
    input()
    driver.quit()


def move_card_or_retry(card_id):
    try:
        move_card_randomly(driver, card_id=card)
    except Exception as e:
        logger.error(str(e))
        move_card_or_retry(card_id)


main()
