import random
import string
import uuid

from src.actions.move_card import move_card_randomly
from src.actions.set_estimation import set_estimation_to_card
from src.actions.types import ActionType
from src.utils.game import get_card_ids


def create_move_action(remaining_ids, placed_cards):
    card_id = remaining_ids.pop(0)
    scope = dict(card=card_id)
    action = {
        'id': str(uuid.uuid4()),
        'type': ActionType.move_card,
        'execute': lambda d: move_card_randomly(d, card_id),
        'scope': scope,
    }
    placed_cards.add(card_id)
    return action


def create_set_estimation_action(cards_ids, placed_cards):
    card_id = random.choice(list(placed_cards))

    estimation = ''.join(random.choice(string.ascii_lowercase) for _ in range(5))
    action = {
        'id': str(uuid.uuid4()),
        'type': ActionType.set_estimation,
        'execute': lambda d: set_estimation_to_card(d, card_id, estimation),
        'scope': dict(card=card_id, estimation=estimation),
    }

    return action


switcher = {
    0: create_move_action,
    1: create_set_estimation_action,
}


def get_game_definition(steps=50):
    game_actions = []
    cards_ids = get_card_ids()
    remaining_ids = [c for c in cards_ids]

    placed_cards = set()

    for i in range(0, steps):
        case = i % 2
        factory = switcher[case]

        action = factory(remaining_ids, placed_cards)

        game_actions.append(action)
    return game_actions
