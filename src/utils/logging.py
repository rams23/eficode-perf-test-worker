import logging
import os
import sys

LOG_DIR = os.path.join(os.path.dirname(__file__), '../../logs')

logging.basicConfig(
    format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s',
)


def get_logger(name):
    file_handler = logging.FileHandler(filename=os.path.join(LOG_DIR, f"{name}.log"))
    stdout_handler = logging.StreamHandler(sys.stdout)

    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    logger.addHandler(file_handler)
    logger.addHandler(stdout_handler)

    return logger
