import math
import random

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


def move_card_by(driver: WebDriver, card_id: str, offset_x: int, offset_y: int, is_already_in_board: bool):
    action = ActionChains(driver)
    card = driver.find_element_by_xpath(f"//div[@data-cy='card-{card_id}']")
    board = driver.find_element_by_xpath(f"//div[@class='board-wrapper']")
    if is_already_in_board:
        action.click_and_hold(card).pause(.5).move_by_offset(offset_x, offset_y).pause(.5).release().perform()
    else:
        action.click_and_hold(card).pause(.5).move_by_offset(offset_x, offset_y).pause(.5).release(board).perform()


def move_card_randomly(driver: WebDriver, card_id: str):
    is_already_in_board = False
    try:
        card = driver.find_element_by_xpath(f"//div[@class='board-wrapper']//div[@data-cy='card-{card_id}']")
        is_already_in_board = True
    except NoSuchElementException:
        card = driver.find_element_by_xpath(f"//div[@data-cy='card-{card_id}']")

    target_x, target_y = find_suitable_new_card_location(driver)
    current_x, current_y = card.location['x'], card.location['y']
    offset_x, offset_y = target_x - current_x, target_y - current_y

    if is_already_in_board:
        # weired offset
        offset_y = offset_y + 100 if is_already_in_board else offset_y
        offset_y = offset_y + 26
        offset_x = offset_x - 107

    move_card_by(driver, card_id, offset_x, offset_y, is_already_in_board)


def first_draw(action, card, card_id, driver):
    board = driver.find_element_by_xpath(f"//div[@class='board-wrapper']")
    try:
        board.find_element_by_xpath(f".//div[@data-cy='card-{card_id}']")
        return False
    except NoSuchElementException:
        o_x = random.randint(-1000, -500)
        o_y = random.randint(-100, -50)
        action.click_and_hold(card).move_to_element(board).pause(.5).move_by_offset(o_x, o_y).release().perform()
        return True


def find_suitable_new_card_location(driver):
    other_placed_cards = driver.find_elements_by_xpath("//div[@class='board-wrapper']//div[contains(@data-cy,'card')]")
    other_card_locations = [(c.location['x'], c.location['y']) for c in other_placed_cards]
    if len(other_placed_cards) == 0:
        return random.randint(50, 800), random.randint(100, 650)

    while True:
        target_x, target_y = (random.randint(50, 800), random.randint(100, 650))
        distances_from_other_cards = [math.sqrt((x - target_x) ** 2 + (y - target_y) ** 2) for x, y in
                                      other_card_locations]

        if all([d > 180 for d in distances_from_other_cards]):
            break
    return target_x, target_y


class CardHasBeenMoved(object):
    """
  locator - used to find the element
  returns the WebElement once its top or left coordinates have changed
  """

    def __init__(self, target_coordinates, locator):
        self.locator = locator
        self.target_x, self.target_y = target_coordinates

    def __call__(self, driver: WebDriver):
        element = driver.find_element(*self.locator)  # Finding the referenced element
        if self.target_x == element.location["x"] - 70 and self.target_y == element.location["y"] + 52:
            return True
        return False


def listen_to_moved_card(driver: WebDriver, card_id: str, target_location):
    wait = WebDriverWait(driver, 100, poll_frequency=0.001, ignored_exceptions=[StaleElementReferenceException])
    wait.until(CardHasBeenMoved(target_location, (By.XPATH, f"//div[@data-cy='card-{card_id}']")))
