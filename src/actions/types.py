from enum import Enum


class ActionType(Enum):
    move_card = 'move_card'
    set_estimation = 'set_estimation'
    job = 'job'
    sign_in = 'sign_in'
