from firebase_admin import auth
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait

from src.utils.game import get_game_url


def sign_up_to_game(driver: WebDriver, game_id: str, email: str, password: str, custom_logger):
    custom_logger.info(f"Signing up {email} to game {game_id}")
    url_to_visit = get_game_url(game_id)
    wait = execute_sign_up(driver, email, password, url_to_visit)

    wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@data-cy='card-sAgOgpSk0phDUfJ7d3Q7']")))



def execute_sign_up(driver, email, password, url_to_visit):
    wait = WebDriverWait(driver, 60, poll_frequency=0.1)
    driver.get(url_to_visit)
    signup_selector = "//button[@id='go-to-signup']"
    wait.until(EC.presence_of_element_located((By.XPATH, signup_selector)))
    got_to_signup_button = driver.find_element_by_xpath(signup_selector)
    got_to_signup_button.click()
    wait.until(EC.presence_of_element_located((By.ID, "firstName")))
    driver.find_element_by_id('firstName').send_keys('John')
    driver.find_element_by_id('lastName').send_keys('Doe')
    driver.find_element_by_id('email').send_keys(email)
    driver.find_element_by_id('password').send_keys(password)
    driver.find_element_by_id('repeatPassword').send_keys(password)
    wait.until(EC.presence_of_element_located((By.XPATH, "//option[@value='endUser']")))
    roles_select = Select(driver.find_element_by_id('role'))
    roles_select.select_by_value('endUser')
    maturities_select = Select(driver.find_element_by_id('devOpsMaturity'))
    maturities_select.select_by_value('veryImmature')
    sign_up_button = driver.find_element_by_xpath("//button[@type='submit']")
    sign_up_button.click()
    wait.until(EC.element_to_be_clickable((By.ID, "resend-email-button")))
    user = auth.get_user_by_email(email)
    # generating the email verification link will result in too_many_attempts error
    auth.update_user(user.uid, email_verified=True)
    driver.get(url_to_visit)
    return wait
