import firebase_admin
from firebase_admin import auth
from firebase_admin import firestore
from firebase_admin.auth import ListUsersPage
from google.cloud.firestore_v1.client import Client

from src.utils.firebase import initialize_admin_sdk

app, firestore_client = initialize_admin_sdk()
page: ListUsersPage = auth.list_users()

for user in page.users:
    if 'perf-test' in user.email or 'pythonsdk' in user.email:
        firestore_client.document(f"users/{user.uid}").delete()
        auth.delete_user(user.uid)
