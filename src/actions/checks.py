from src.actions.move_card import listen_to_moved_card
from src.actions.set_estimation import listen_to_estimation_set
from src.actions.types import ActionType

check_functions = {
    ActionType.move_card: lambda driver, params: listen_to_moved_card(driver, params["card"]),
    ActionType.set_estimation: lambda driver, params: listen_to_estimation_set(driver, params["card"],
                                                                               params["estimation"])
}
