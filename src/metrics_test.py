import json
import os
import time

from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver

from src.actions.move_card import find_suitable_new_card_location
from src.actions.sign_in_to_game import sign_in_to_game
from src.game_definition import get_game_definition
from src.jobs.coordinator import calculate_metrics
from src.jobs.facilitator_job import try_really_really_hard
from src.utils.selenium import initialize_driver
ALL_USERS = os.path.join(os.path.dirname(__file__), '../data/all-users.json')


def calculate_diffs_of_users_event_received():
    with open(ALL_USERS, 'r') as f:
        e = json.load(f)
        ref_key = list(e.keys())[0]
        ref = e[ref_key]

        diffs = dict()

        for key in [k for k in e.keys() if k != ref_key]:
            user_timestamps = e[key]
            for event_id in user_timestamps.keys():
                if event_id not in diffs:
                    diffs[event_id] = []
                deltas = diffs[event_id]
                deltas.append(ref[event_id]- user_timestamps[event_id])

        return diffs

metrics = dict()
calculate_metrics(metrics)
print(metrics)

users_diffs = calculate_diffs_of_users_event_received()
print(users_diffs)
