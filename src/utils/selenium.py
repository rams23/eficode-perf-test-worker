import os

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


def initialize_driver():
    # first run "docker-compose up" to launch the webdrivers
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--whitelisted-ips")
    chrome_options.add_argument("--window-size=1920,1080")

    desired_capabilities = DesiredCapabilities.CHROME
    desired_capabilities['goog:loggingPrefs'] = {'browser': 'ALL'}

    local_selenium = os.getenv("USE_LOCAL_SELENIUM")
    if local_selenium == 'true':
        return get_local_selenium(chrome_options, desired_capabilities)
    else:
        return get_dockerized_selenium(chrome_options, desired_capabilities)


def get_local_selenium(options, capabilities):
    return webdriver.Chrome(
        executable_path='chromedriver.exe',
        desired_capabilities=capabilities,
        options=options)


def get_dockerized_selenium(options, capabilities, selenium_docker_port=5005):
    return webdriver.Remote(
        command_executor=f'http://localhost:{selenium_docker_port}/wd/hub',
        desired_capabilities=capabilities,
        options=options)
