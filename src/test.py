import time

from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver

from src.actions.move_card import find_suitable_new_card_location
from src.actions.sign_in_to_game import sign_in_to_game
from src.game_definition import get_game_definition
from src.jobs.facilitator_job import try_really_really_hard
from src.utils.selenium import initialize_driver


my_driver = initialize_driver()

sign_in_to_game(my_driver, 'U0zLyWj74a5AihiukLOI', 'test@test.com', 'Test1234')

my_driver.find_element_by_id('zoom-dial').click()
time.sleep(1)
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-out-button').click()
my_driver.find_element_by_id('zoom-dial').click()

# move_card_by(my_driver, '9tzSep5ORXJoylxVlb6M', -200, -200)
# time.sleep(2)
# move_card_by(my_driver, 'gFDFKdnMmbwL5Vt7ouDE', -500, -200)

# http://localhost:3000/game/U0zLyWj74a5AihiukLOI
for a in get_game_definition(20):
    print(f"Performing {a['type']} on {a['scope']}")
    try_really_really_hard(lambda: a["execute"](my_driver), attempts=10)
    time.sleep(0.5)
